var Lattice = Lattice || {};

Lattice.debug = (function () {
    'use strict';

    function drawGuides(ctx, lineWidth) {
        var canv = ctx.canvas,
            hWidth = canv.width / 2,
            hHeight = canv.height / 2,
            oWidth = ctx.lineWidth,
            oStroke = ctx.strokeStyle;
        ctx.beginPath();
        ctx.strokeStyle = 'yellow';
        ctx.lineWidth = lineWidth || canv.height / 100;
        ctx.moveTo(hWidth, 0);
        ctx.lineTo(hWidth, canv.height);
        ctx.moveTo(0, hHeight);
        ctx.lineTo(canv.width, hHeight);
        ctx.stroke();
        ctx.lineWidth = oWidth;
        ctx.strokeStyle = oStroke;
    }

    function drawCircle(ctx, x, y, colour, radius) {
        var oFill = ctx.fillStyle;
        colour = colour || 'gray';
        radius = radius || 1;
        ctx.beginPath();
        ctx.fillStyle = colour;
        ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.fillStyle = oFill;
    }

    return {
        drawGuides: drawGuides,
        drawCircle: drawCircle
    };
}());



Lattice.main = (function () {
    'use strict';

    var settings,
        canvLattice,        // on-screen canvas
        ctxLattice,         // on-screen canvas context
        hWidth,             // half lattice width
        hHeight,            // half lattice height
        latticeHeight,
        latticeWidth,
        divisions,          // number of lines in final lattice
        numLines,           // number of lines to draw for copying
        strokeWidth,
        yOffset,            // y co-ord of topmost line
        xOffset,            // x co-ord of topmost line
        cp1x,
        cp2x,
        cp1Offset,          // topmost control point
        cp1Power,           // defines distribution of points
        cp1PMax,            // bottommost control point
        cp2Offset,          // topmost control point
        cp2Power,           // defines distribution of points
        cp2PMax;            // bottommost control point


    // TODO: split size-dependent code off into separate function
    // so we can dynamically re-size more efficiently
    function defineLattice() {
        // size canvas to square
        canvLattice.width = settings.size;
        canvLattice.height = settings.size;

        // define square lattice drawing area
        latticeHeight = settings.size;
        latticeWidth = settings.size;

        hWidth = hHeight = settings.size / 2;

        divisions = settings.divisions;
        numLines = Math.floor(divisions / 2);

        // define topmost line
        yOffset = xOffset = latticeHeight / settings.strokeRatio;
        strokeWidth = yOffset;
        if (strokeWidth < 1) {
            // prevent stroke width from being vanishingly small
            strokeWidth = 1;
        }

        // define first control point
        cp1Offset = yOffset;
        cp1Power = settings.cp1Power;
        cp1PMax = Math.pow(numLines, cp1Power);
        cp1x = latticeWidth / 2 * settings.cp1ratioX;

        // define second control point
        cp2Offset = latticeHeight / 10;
        cp2Power = settings.cp2Power;
        cp2PMax = Math.pow(numLines, cp2Power);
        cp2x = hWidth - (latticeWidth / 2 * settings.cp2ratioX);
    }


    // http://stackoverflow.com/a/5295202
    function scaleNumberInRange(origVal, origMin, origMax, targetMin, targetMax) {
        return (((targetMax - targetMin) * (origVal - origMin)) / (origMax - origMin)) + targetMin;
    }


    /*
     * Get the co-ordinate at `t` along the straight line between two points,
     * where `t` is the distance along the line to calculate the point at:
     * t = 0   = start of line
     * t = 0.5 = midway along line
     * t = 1   = end of line
     */
    function getMidpoint(t, p1x, p1y, p2x, p2y) {
        var dX,
            dY,
            midX,
            midY;

        dX = p2x - p1x;
        dY = p2y - p1y;
        midX = p1x + t * dX;
        midY = p1y + t * dY;

        return {
            x: midX,
            y: midY
        };
    }


    /*
     * @param {Number} t Range 0 to 1.
     * @param {Array} pointArray Array of point objects eg [{x: 1, y: 1}, {x: 20, y: 30}]
     * @returns {Array} Array of points. Length is one less than the length of the passed point array.
     */
    function subdivideMidpoints(t, pointArray) {
        var retVal,
            len,
            i;

        len = pointArray.length;

        if (len < 2) {
            return pointArray;
        }

        retVal = [];

        for (i = 0; i < len - 1; i += 1) {
            retVal.push(getMidpoint(t, pointArray[i].x, pointArray[i].y, pointArray[i + 1].x, pointArray[i + 1].y));
        }

        return retVal;
    }


    /*
     * Use De Casteljau's algorithm to find a point on a Bezier
     * Returns an object in the form of {x: 20, y: 70}
     * Parameter `t` is the distance along the Bezier to calculate the point at, where
     * t = 0   = start of curve
     * t = 0.5 = midway along curve
     * t = 1   = end of curve
     * http://processingjs.nihongoresources.com/decasteljau/
     * http://processingjs.nihongoresources.com/decasteljau/BezierCurve.pde :: subdivide()
     */
    function getPointOnBezier(t, xStart, yStart, cp1x, cp1y, cp2x, cp2y, xEnd, yEnd) {
        var pointArray;

        pointArray = [
            {x: xStart, y: yStart},
            {x: cp1x, y: cp1y},
            {x: cp2x, y: cp2y},
            {x: xEnd, y: yEnd}
        ];

        // subdivideMidpoints() returns an array with one less item in it each time
        // keep feeding subdivideMidpoints() its last result until there's only one value left
        // this point will be on the Bezier at `t` along the curve
        while (pointArray.length > 1) {
            pointArray = subdivideMidpoints(t, pointArray);
        }

        return pointArray[0];
    }


    function computeMode(i) {
        var t,
            oMin, // original minimum value
            x,
            y,
            cp1y,
            cp2y;

        oMin = 0;

        switch (settings.mode) {
        case 'flat':
            x = 0;
            y = scaleNumberInRange(i, oMin, numLines, yOffset, hHeight);
            cp1y = y;
            cp2y = y;
            break;

        case 'petals':
            x = 0;
            y = 0;
            cp1y = scaleNumberInRange(Math.pow(i, cp1Power), oMin, cp1PMax, cp1Offset, hHeight);
            cp2y = scaleNumberInRange(Math.pow(i, cp2Power), oMin, cp2PMax, cp2Offset, hHeight);
            break;

        case 'bulgeOut':
            // `t` is the distance along the Bezier we are currently at
            t = (i - 1) / numLines;
            x = getPointOnBezier(t, xOffset, yOffset, cp1x, cp1Offset, cp2x, cp2Offset, hWidth, cp2Offset).y;
            y = scaleNumberInRange(i, oMin, numLines, yOffset, hHeight);
            cp1y = scaleNumberInRange(Math.pow(i, cp1Power), oMin, cp1PMax, cp1Offset, hHeight);
            cp2y = scaleNumberInRange(Math.pow(i, cp2Power), oMin, cp2PMax, cp2Offset, hHeight);
            break;

        case 'plus':
            // `t` is the distance along the Bezier we are currently at
            t = (i - 1) / numLines;
            x = getPointOnBezier(t, xOffset, yOffset, cp1x, cp1Offset, cp2x, cp2Offset, hWidth, cp2Offset).y;
            y = scaleNumberInRange(i, oMin, numLines, yOffset, hHeight);
            cp1y = scaleNumberInRange(Math.pow(i, cp1Power), oMin, cp1PMax, y, hHeight);
            cp2y = scaleNumberInRange(Math.pow(i, cp2Power), oMin, cp2PMax, y, hHeight);
            break;
        }

        // use bitwise shift to round up/down
        x = ~~x;
        y = ~~y;
        cp1y = ~~cp1y;
        cp2y = ~~cp2y;

        return {
            x: x,
            y: y,
            cp1y: cp1y,
            cp2y: cp2y
        };
    }


    // draws a quarter of the lattice along one plane
    function renderTile(ctx) {
        var i,
            modeValues;
        
        ctx.lineWidth = strokeWidth;
        ctx.strokeStyle = settings.strokeColour;
        // ctx.lineCap = 'round';

        for (i = 0; i <= numLines; i += 1) {
            if (i === 0) {
                // skip outermost line
                continue;
            }

            modeValues = computeMode(i);

            ctx.beginPath();
            ctx.moveTo(modeValues.x, modeValues.y);
            ctx.bezierCurveTo(cp1x, modeValues.cp1y, cp2x, modeValues.cp2y, hWidth, modeValues.cp2y);
            ctx.stroke();

            // debug
            // Lattice.debug.drawCircle(ctx, modeValues.x, modeValues.y, 'red');
            // Lattice.debug.drawCircle(ctx, cp1x, modeValues.cp1y, 'green');
            // Lattice.debug.drawCircle(ctx, cp2x, modeValues.cp2y, 'blue');
        }
    }


    // draws the lattice a quarter at a time along one plane
    function renderPlane(ctx) {
        // top-left
        renderTile(ctx);

        // top-right
        ctx.save();
        ctx.scale(-1, 1);
        ctx.translate(-canvLattice.width, 0);
        renderTile(ctx);
        ctx.restore();

        // bottom-left
        ctx.save();
        ctx.scale(1, -1);
        ctx.translate(0, -canvLattice.height);
        renderTile(ctx);
        ctx.restore();

        // bottom-right
        ctx.save();
        ctx.scale(-1, -1);
        ctx.translate(-canvLattice.width, -canvLattice.height);
        renderTile(ctx);
        ctx.restore();
    }


    function renderLattice(ctx) {
        // clear canvas
        canvLattice.width = canvLattice.width;

        // draw horizontal lines
        renderPlane(ctx);

        ctx.save();
        // rotate canvas to draw vertical lines
        ctx.translate(ctx.canvas.width, 0);
        ctx.rotate(Math.PI * 0.5);
        // draw horizontal lines on the rotated canvas
        renderPlane(ctx);
        ctx.restore();
    }


    function init(initObj) {
        initObj = initObj || {};
        settings = {};

        // strokeRatio defines relative thickness of lines to the size of the canvas
        // cp1Power defines the distribution of first control points
        // cp2Power defines the distribution of second control points
        // cp1ratioX defines distance from left-hand edge to first control point
        // cp2ratioX defines distance from lattice centre to second control point
        // mode changes the pattern drawn: 'flat', 'bulgeOut', 'petals'
        settings.size         = initObj.size || 400;
        settings.divisions    = initObj.divisions || 18;
        settings.strokeColour = initObj.strokeColour || 'grey';
        settings.strokeRatio  = initObj.strokeRatio || 140;
        settings.cp1Power     = initObj.cp1Power || 1.5;
        settings.cp2Power     = initObj.cp2Power || 2.2;
        settings.parent       = initObj.parent || document.getElementsByTagName('body')[0];
        settings.cp1ratioX    = initObj.cp1ratioX || 0.5;
        settings.cp2ratioX    = initObj.cp2ratioX || 0.3;
        settings.mode         = initObj.mode || 'flat';

        canvLattice = document.createElement('canvas');
        ctxLattice = canvLattice.getContext('2d');

        canvLattice.id = initObj.id || 'lattice';
        settings.parent.appendChild(canvLattice);

        defineLattice();
        renderLattice(ctxLattice);
    }


    return {
        init: init
    };

}());
